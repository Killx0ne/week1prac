# Task 0

Clone this repository (well done!)

# Task 1

Take a look a the two repositories:
  
  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A?

    Ans: farleyknight

  * Who made the first commit to repository A?

    Ans: scott Chacon

  * Who made the last and first commits to repository B?

    Ans: Shawn O. Pearce made the last and The Android Open Source Project made the first commits to it.
  
* Who has been the most active recent contributor on repository A?  How about repository B?

    Ans: Farley Knight is the most recent active contributor on repository A though Scott Chacon is the most active contributor of all time but he stopped contributing after 2009. He has no recent activities. 

    Shawn O. Pearce is the most recent active contributor on repository B
  
* Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?

    Ans: Both of these projects are inactive at this moment. Both projects were active till 2012 but after that these became inactive. I think both of these projects were done thats why they're not active anymore.
  
* 🤔 Which file in each project has had the most activity?

    Ans: On repository A, lib.rb (lib/git/lib.rb) had the most activity.
        On repository B, project.py had the most activity. 

# Task 2

Setup a new eclipse project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~ 

🤔 Now setup a new bitbucket repository and have this project pushed to that repository.  You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore` file.  The one we have in this repository is a very good start.